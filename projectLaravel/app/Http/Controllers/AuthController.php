<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AuthController extends Controller{
    //Register View
    public function pendaftaran(){
        return view('Register');
    }

    //Welcome View
    public function member(Request $request){
       // dd($request->all());
       $first = $request["first"];
       $last = $request["last"];
       return view('Welcome', compact("first", "last"));
    }
}