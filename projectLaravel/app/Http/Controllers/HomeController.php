<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class HomeController extends Controller{
    //Home View
    public function dasboard(){
       return view("home");
    }
    public function tampilanHome(){
        return view("tampilanHome");
    }
    public function kontaksaya(){
        return view("kontak");
    }
}