<?php

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-tables', function(){
    return view('table.data-tables');
});

Route::get('/homepage', 'HomeController@tampilanHome');
Route::get('/kontak', 'HomeController@kontaksaya');

Route::get('/', 'HomeController@dasboard');
Route::get('/register', 'AuthController@pendaftaran');
Route::post('/welcome', 'AuthController@member');


    //CRUD Cast
    //Mengarah ke form create data
    Route::get('/cast/create', 'CastController@create');
    //Menyimpan data ke table Cash
    Route::post('/cast', 'CastController@store');
    //Read data
    Route::get('/cast', 'CastController@index');
    //Detail data
    Route::get('/cast/{cast_id}', 'CastController@show');
    //Edit form data
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');
    //Update data Cast
    Route::put('/cast/{cast_id}', 'CastController@update');
    //Delete Data Cast
    Route::delete('/cast/{cast_id}', 'CastController@destroy');

?>