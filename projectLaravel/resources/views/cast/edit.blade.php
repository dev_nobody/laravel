@extends('layout.master')

@section('judul')
    Edit Cast
@endsection

@section('title')
    Form Edit Data Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" value="{{$cast->nama}}" class="form-control" name= "nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="number" value="{{$cast->umur}}" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-danger ml-5">Batalkan</a>
  </form>
    
@endsection