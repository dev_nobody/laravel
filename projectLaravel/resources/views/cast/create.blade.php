@extends('layout.master')

@section('judul')
    Data Cast
@endsection

@section('title')
    Form Tambah Data Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control" name= "nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
   
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-warning ml-5">Kembali</a>
  </form>
    
@endsection