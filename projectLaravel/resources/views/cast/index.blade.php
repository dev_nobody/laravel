@extends('layout.master')

@section('judul')
    Data Cast
@endsection

@section('title')
    Halaman Data Cast
@endsection

@section('content')

    <a href="/cast/create" class="btn btn-primary">Tambah Data Cast</a><br><br>
    <table class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Cast</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
       @forelse ($cast as $key=>$item)
           <tr>
               <td>{{$key  + 1}}</td>
               <td>{{$item->nama}}</td>
               <td>{{$item->umur}}</td>
               <td>{{$item->bio}}</td>
               <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                   <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                   <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm ml-3">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm ml-3" value="Delete">
                   </form>
               </td>
           </tr>
       @empty
           <h1>Data Kosong</h1>
       @endforelse
    </tbody>
    </table>
@endsection