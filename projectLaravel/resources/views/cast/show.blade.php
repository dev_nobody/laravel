@extends('layout.master')

@section('judul')
    Detail Data Cast
@endsection

@section('title')
    Halaman Detail dari Data Cast
@endsection

@section('content')
    <h2>{{$cast->nama}}/{{$cast->umur}} tahun.</h2>
    <p>{{$cast->bio}}</p><br>

    <a href="/cast" class="btn btn-primary">Kembali</a>
@endsection