@extends('layout.master')
@section('judul')
    Halaman Pendaftaran
@endsection

@section('title')
    Form Registration
@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="first" required><br><br>

        <label>Last Name:</label><br>
        <input type="text" name="last" required><br><br>

        <label>Gender: </label><br>
        <input type="radio" name="jk" required>Man<br>
        <input type="radio" name="jk" required>Woman<br>
        <input type="radio" name="jk" required>Other<br><br>

        <label>Nationality: </label>
        <select country="negara" required>
            <option value="Indonesia">Indonesia</option>
            <option value="Singapura">Singapura</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option> 
        </select><br><br>

        <label>Language Spoken: </label><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Arabic<br>
        <input type="checkbox" name="language">Japanese<br><br>

        <label>Bio: </label><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="Sign Up"></a>

    </form>
@endsection